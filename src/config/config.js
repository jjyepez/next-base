import {
    User,
    Users,
    Meh,
    Server,
    Home,
    Maximize,
    MessageCircle,
    Layers
} from 'react-feather';

const iconOptions = {
    strokeWidth: 1,
    size: 18,
    style: {
        marginBottom: '-3px'
    }
}

export const config = {
    app: {

        theme      : 'default',    // no está en uso
        
        version    : '1.0b',
        title      : 'DEMO base',
        shortname  : 'demo-base',
        description: 'Descripción de la aplicación demo-base.',
        logoUrl    : '/img/logo.svg',
        faviconUrl : '/img/favicon.svg',
        
        homeRoute  : '/dashboard',
        loginRoute : '/login',
        
        modules: {

            '/dashboard': {
                icon  : <Home {...iconOptions} />,
                text  : 'Inicio',
                layout: 'full-layout',

                submodules: {
                    '/dashboard': {
                        icon: <Server {...iconOptions} />,
                        text: 'Dashboard',
                        layout: 'sidebar-layout'
                    }
                }
            },

            '/module': {
                
                icon  : <Layers {...iconOptions} />,
                text  : 'Módulo 01',
                layout: 'full-layout',
            
                submodules: {
                    '/module/sub': {
                        icon: <Maximize {...iconOptions} />,
                        text: 'Sub módulo',
                        layout: 'sidebar-layout'
                    },
                }
            }
        }
    }
}

export default config;