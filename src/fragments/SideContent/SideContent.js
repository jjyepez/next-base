import EditableQueryGroup from "@components/EditableTagGroup/EditableTagGroup";

import { PageHeader } from "antd";

import styles from './SideContent.module.less'
import { Button } from "antd";

const SideContent = () => {

  return (
    <div className={styles.SideContent}>
      
      <PageHeader
        title="Consulta"
      />

      <form>
        <div className='form-group'>
          <div className={styles.EditableQueryBox}>
            <EditableQueryGroup/>
          </div>
        </div>

        <Button className={styles.Button}>
          Consultar
        </Button>

      </form>

    </div>
  )
  
}

export default SideContent;