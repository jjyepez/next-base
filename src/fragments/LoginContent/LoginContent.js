import { useState, useContext } from "react";
import { useCookies } from "react-cookie";
import Router from 'next/router';

import config from '@config/config';

import {
    SET_SESSION,
    DESTROY_SESSION
} from '@context/AppContext/Actions';

const { app } = config;

import {
    Form,
    Input,
    Button,
    Layout,
    Card,
    Alert
} from 'antd';

import auth from "@services/auth";

import styles from "./LoginContent.module.less";

const { Content } = Layout;
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import AppContext from '@context/AppContext/Context';

const LoginContent = () => {

    const appContext = useContext(AppContext);

    const [warning, setWarning] = useState('');
    const [cookie, setCookie] = useCookies(["IUEtoken"]);
    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        handleSignIn(values.uid, values.pwd);
    }

    const handleSignIn = async (uid, pwd) => {
        try {
            setWarning('');
            const response = await auth.user(uid, pwd); //handle API call to sign in here.
            if(response.status === 200) {
                const payload = response.payload;

                setCookie("IUEtoken", JSON.stringify(payload), {
                    path: "/",
                    maxAge: 2592000, // Expires after 30D
                    sameSite: 'lax',
                });

                Router.push('/');

                appContext.dispatch({type: SET_SESSION, payload});
            } else {
                setWarning('Credenciales inválidas.');
            }

        } catch (err) {
            setWarning('Error iniciando sesión.');
            console.log(err);
        }
    }

    return (
        <Content className={styles.LoginContent}>

            <div className={styles.cardWrap}>

                <Card title="Inicio de Sesión" bordered={true} className={styles.Card}>

                    {warning && (
                        <Alert
                            className={styles.Alert}
                            message={warning}
                            type="error"
                            showIcon
                            closable
                        />
                    )}

                    <Form
                        form={form}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            className={styles.FormItem}
                            name='uid'
                            rules={[{ required: true, message: 'Debe indicar nombre de usuario.' }]}
                        >
                            <Input
                                //size='large'
                                prefix={<UserOutlined />}
                                placeholder="Usuario"
                            />
                        </Form.Item>

                        <Form.Item
                            className={styles.FormItem}
                            name='pwd'
                            rules={[{ required: true, message: 'Debe indicar contraseña.' }]}
                        >
                            <Input
                                //size='large'
                                prefix={<LockOutlined />}
                                type='password'
                                placeholder="Contraseña"
                            />
                        </Form.Item>

                        <Form.Item>
                            <Button
                                className={styles.FormItem}
                                htmlType="submit"
                                type="primary"
                                size='large'
                                block
                            >Acceder</Button>
                        </Form.Item>

                    </Form>

                </Card>

            </div>

            <div className={styles.appDescription}>
                <p>
                    {app.description}
                </p>
            </div>
        
        </Content>
    );
}

export default LoginContent;