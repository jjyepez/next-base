import Link from 'next/link';

import config from '@config/config';

import styles from './AppLogo.module.less';

function AppLogo(){
    return (
        <Link className="AppLogo" href={config.app.homeRoute}>
            <a>
                <img className={styles.LogoImage} src={config.app.logoUrl} />
            </a>
        </Link>
    )
}

export default AppLogo;