import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";

import Router from "next/router";
import DivLoading from "@components/DivLoading/DivLoading";
import FlexContainer from "@components/FlexContainer/FlexContainer";

const AuthProvider = ({ loginRoute = "/login", homeRoute = "/", children }) => {
  const [cookies] = useCookies(["IUEtoken"]);
  const [authenticating, setAuthenticating] = useState(true);

  useEffect(async () => {
    const onLoginPage = (Router.pathname === loginRoute);

    if (!cookies.IUEtoken && !onLoginPage) {
      await Router.push(loginRoute);
    }

    if (cookies.IUEtoken && onLoginPage) {
      await Router.push(homeRoute);
    }

    setAuthenticating(false);
  }, [cookies.IUEtoken]);

  return authenticating ? (
    <FlexContainer>
      <DivLoading text="Cargando 3." />
    </FlexContainer>
  ) : (
    { ...children }
  );
};

export default AuthProvider;
