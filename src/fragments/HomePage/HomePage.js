import { useEffect } from 'react';
import Router from 'next/router';

import DivLoading from '@components/DivLoading/DivLoading';

import { parseCookies } from "@helpers/cookies";

import styles from "./HomePage.module.less";

const HomePage = (req, res) => {

    const { IUEtoken = null } = req.data;
    console.log(IUEtoken, req);

    let newHomeRoute = '/login';
    if (IUEtoken) {
        newHomeRoute = '/incidente-unico';
    };

    useEffect(() => {
        const { pathname } = Router;
        if (pathname == '/') {
            Router.push(newHomeRoute);
        }
    }, []);

    return (
        <>
            <div className={styles.homePage}>
                <div>
                    <DivLoading text='Cargando 4.' style='default' />
                </div>
            </div>
        </>
    )
}

HomePage.getInitialProps = async ({ req, res }) => {

    const data = parseCookies(req);

    console.log({data});

    if (res) {
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            console.log({IN:data});
            res.writeHead(301, {
                Location: "/login"
            })
            .end();
        }
    }

    return {
        data: data && data,
    }
}

export default HomePage;
