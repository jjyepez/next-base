import { useEffect } from 'react';
import { useRouter } from 'next/router';

import config from '@config/config';

export default function (){
    
    const router = useRouter();
    
    useEffect( () => {
        if(config.app.modules.hasOwnProperty(router.pathname)){
            const submodules = Object.keys(config.app.modules[router.pathname]?.submodules || {});
            if(submodules.length > 0){
                router.push(submodules[0]);
            }
        }
    }, [router]);

    return (
        <>
            <div>
                {router.pathname}
            </div>
        </>
    )

}
