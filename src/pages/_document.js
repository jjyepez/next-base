import Document, { Html, Head, Main, NextScript } from "next/document";
import config from "@config/config";

class MyDocument extends Document {
  render() {
    return (

      <Html>
      
        <Head>

          <link rel="shortcut icon" href={config.app.faviconUrl} />

        </Head>

        <body>

          <Main />

          <NextScript />

        </body>

      </Html>
    );
  }
}

export default MyDocument;
