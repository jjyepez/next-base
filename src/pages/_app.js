import { CookiesProvider } from "react-cookie";

import { ConfigProvider } from 'antd';
import esES from 'antd/lib/locale/es_ES';

import ContextProvider from '@context/AppContext/Provider';
import AuthProvider from "@fragments/AuthProvider/AuthProvider";

import "antd/lib/style/color/colors.less";
import '../styles/styles.less';
import '../styles/themes/dark.less';

import Head from 'next/head';
//import App from 'next/app';

import FlexLayout from "@components/FlexLayout/FlexLayout";

import config from "@config/config";
const { app } = config;

function MyApp({ Component, pageProps }) {

  const title = `${app.title} v${app.version}`;

  return (
    <ContextProvider>

      <ConfigProvider locale={esES}>

        <CookiesProvider>

          <Head>
            <title>{app.shortname} v{app.version}</title>
          </Head>

          {/* TODO: El loader de AuthProvider antes de montar los componentes no contiene MenuBar */}
          <AuthProvider loginRoute={app.loginRoute} homeRoute={app.homeRoute}>

            <FlexLayout title={title} modules={app.modules} loginRoute={app.loginRoute}>

              <Component {...pageProps} />

            </FlexLayout>

          </AuthProvider>

        </CookiesProvider>

      </ConfigProvider>

    </ContextProvider>

  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.

// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);

//   console.log({appProps});

//   return {...appProps}
// }

export default MyApp;
