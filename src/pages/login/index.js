import LoginContent from "@fragments/LoginContent/LoginContent";

const LoginPage = () => {
  return (
    <>
      <LoginContent />
    </>
  );
};

export default LoginPage;
