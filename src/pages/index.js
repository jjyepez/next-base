import { useEffect } from 'react';
import Router from 'next/router';

import DivLoading from '@components/DivLoading/DivLoading';

import { parseCookies } from "../helpers/cookies";
import FlexContainer from '@components/FlexContainer/FlexContainer';

import config from '@config/config';

HomePage.getInitialProps = async ({ req, res }) => {
    // ESTO SUCEDE EN EL SERVIDOR ---- analizar

    const data = parseCookies(req);

    if (res) {
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            
            console.log({IN:data});
            
            res.writeHead(301, {
                Location: config.app.loginRoute
            })
            .end();
        }
    }

    return {
        data: data && data,
    }
}

function HomePage(req, res){

    const { IUEtoken = null } = req.data;
    // console.log(IUEtoken, req);

    let newHomeRoute = config.app.loginRoute;
    if (IUEtoken) {
        newHomeRoute = config.app.homeRoute;
    };

    useEffect(() => {
        const { pathname } = Router;
        if (pathname == '/') {
            Router.push(newHomeRoute);
        }
    }, []);

    return (
        <>
          <FlexContainer>
            <DivLoading text='Cargando 1.' style='default' />
          </FlexContainer>
        </>
    )
}

export default HomePage;
