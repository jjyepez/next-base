import { useRouter } from 'next/router';

export default function (){
    
    const route = useRouter();

    return (
        <>
            <div>
                {route.pathname}
            </div>
        </>
    )

}
