import {
    Loader
} from 'react-feather';

import { Spin } from 'antd';

import styles from './DivLoading.module.less';

function DivLoading({text='', style='default', size='2.5rem'}) {

    return (
    
        <div className={styles.DivLoading}>
    
            <div>

                {style === 'antd' ? (
                    <Spin size='large' tip={text} />
                ) : (
                    <>
                        <Loader className='spin' size={size} strokeWidth={1} />
                        <div className={styles.LoadingText}>{text}</div>
                    </>
                )}
                
            
            </div>
    
        </div>
    
    )
}

export default DivLoading;
