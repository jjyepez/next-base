import { Loader } from "react-feather";

import styles from "./SpinLoader.module.less";

const SpinLoader = ({ type = 'default', text = '', loading }) => (
  <div className={styles.wrap}>
    { loading ?
      <Loader className={styles[type]}/>
      :
      text
    }
  </div>
);

export default SpinLoader;
