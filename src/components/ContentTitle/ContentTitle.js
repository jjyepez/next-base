import { PageHeader } from "antd";

import CrudButtons from '@components/CrudButtons/CrudButtons';

function ContentTitle({title, total, isCrud, newRecordHandle}){

    return (

        <PageHeader
            title={title}
            subTitle={`(${total})`}
            className=''

            extra = { isCrud && <CrudButtons newRecordHandle={newRecordHandle}/> }
        >
            
        </PageHeader>
    
    )
}

export default ContentTitle;
