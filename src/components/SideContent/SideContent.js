import styles from './SideContent.module.less';

function SideContent({children,...props}){
    return (
        <div className={styles.SideContent}>
            {children}
        </div>
    )
}

export default SideContent;