import { useEffect } from 'react';

import { CRS, Icon } from 'leaflet';
import "leaflet/dist/leaflet.css";

import { MapContainer, TileLayer, Marker, Popup, Circle, Tooltip, WMSTileLayer } from 'react-leaflet';

import styles from './Map.module.less';
import Toolbar from '@components/Toolbar/Toolbar';

const position = [4.60931910300002, -74.185823456];

export const Map = ({ toolbar=false, toolbarButtons=[] }) => {

    return (
        <>
            <MapContainer
                className={styles.MapContainer}
                center={position}
                zoom={12}
                maxZoom={18}
                scrollWheelZoom={true}
                tileSize={512}
                fadeAnimation={false}
                nocrs={CRS.EPSG4326}
            >
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    nourl="http://mt3.google.com/vt/lyrs=r&x={x}&y={y}&z={z}"
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                
                {/* <WMSTileLayer
                    opacity={.15}
                    transparent={true}
                    
                    url='http://ows.mundialis.de/services/service?'
                    layers='SRTM30-Colored-Hillshade'
                /> */}

                <Circle center={position} radius={200} fillColor='red' fillOpacity={.15} weight={2} color='red' />

                <Marker
                    position={position}
                    draggable={false}
                    icon={new Icon({
                        iconSize: [30, 40],
                        iconAnchor: [15, 40],
                        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg',
                        popupAnchor: [0, -40]
                    })}
                >
                    <Tooltip>
                        This is the tooltip
                    </Tooltip>

                    <Popup>
                        A pretty CSS3 popup. <br /> Easily customizable.
                    </Popup>

                </Marker>

            </MapContainer>
            
            {toolbar && <Toolbar buttons={toolbarButtons} />}
        </>
    )
}

export default Map;
