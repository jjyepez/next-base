module.exports = {
    
    map: {
        defaultZoom: 12,
        maxZoom: 18,
        defaultLocation: [4.65432,-74.1234]
    },

    baseMapLayers: [
        {
            name: 'OpenStreetMap',
            checked: true,
            url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            attribution: 'OpenStreetMap &reg; 2021'
        },
        {
            name: 'Google',
            url: 'http://mt3.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',
            attribution: 'Google Maps &reg; 2021'
        },
        {
            name: 'WMF Labs',
            url: 'http://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
            attribution: 'WMF Labs &reg; 2021'
        },
        {
            name: 'Stamen',
            url: 'http://a.tile.stamen.com/toner-lite/{z}/{x}/{y}.png',
            //url: 'http://tile.stamen.com/toner/{z}/{x}/{y}.png',
            attribution: 'Stamen &reg; 2021'
        },
        {
            name: 'IDECA dark',
            url: 'https://serviciosgis.catastrobogota.gov.co/arcgis/rest/services/Mapa_Referencia/mapa_base_oscuro_3857/MapServer/tile/{z}/{y}/{x}',
            //url: 'https://serviciosgis.catastrobogota.gov.co/arcgis/rest/services/Mapa_Referencia/mapa_base_gris/MapServer/tile/{z}/{y}/{x}',
            //url: 'https://serviciosgis.catastrobogota.gov.co/arcgis/rest/services/Mapa_Referencia/mapa_base_3857/MapServer/tile/{z}/{y}/{x}',
            attribution: 'IDECA &reg; 2021'
        }
    ]
}
