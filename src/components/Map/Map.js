import "leaflet/dist/leaflet.css";

import {
    LayersControl,
    MapContainer,
    TileLayer,
    useMap
} from 'react-leaflet';

const { BaseLayer } = LayersControl;

import styles from './Map.module.less';
import config from "./config";

export const Map = ({extraLayersRenderer, dynamicMarkers=()=>{}, ...props}) => {

    return (
        <>

            <MapContainer
                className={styles.MapContainer}

                center={config.map.defaultLocation}
                zoom={config.map.defaultZoom}
                maxZoom={config.map.maxZoom}
                
                scrollWheelZoom={true}
                tileSize={512}
                fadeAnimation={false}
            >

                <LayersControl>

                    {config.baseMapLayers.map((baseMap, i) => {
                        return (
                            <BaseLayer
                                key={i}
                                checked={baseMap.checked}
                                name={baseMap.name}
                            >
                                <TileLayer {...baseMap} />
                            
                            </BaseLayer>
                        )
                    })}

                    {extraLayersRenderer && extraLayersRenderer()}

                </LayersControl>

                { dynamicMarkers && dynamicMarkers() }

            </MapContainer>

        </>
    )
}

export default Map;
