import { useEffect, useState } from "react";

import { Menu } from "antd";

import Link from 'next/link';
import { useRouter } from "next/router";

import styles from './TabNav.module.less';

const TabNav = ({ modules = {} }) => {
   
   const router = useRouter();
   const modulePath = `/${router.pathname.split('/')[1]}`;
   const [current, setCurrent] = useState(modulePath);

   useEffect(() => {
      // console.log({modulePath});
      setCurrent(modulePath);
   }, [modulePath])

   return (
      <Menu
         mode='horizontal'
         selectedKeys={[current]}
         className={styles.Menu}
      >

         {
            Object.keys(modules).map((module, i) => {

            return (

               <Menu.Item key={module} icon={modules[module].icon}>

                  <Link href={module} passHref><a>{modules[module].text}</a></Link>

               </Menu.Item>

            )
         })}

      </Menu>
   )
}

export default TabNav;
