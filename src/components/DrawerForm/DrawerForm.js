import { useState, useEffect } from 'react';

import {
  Drawer,
  Form,
  Button,
} from 'antd';

import FormFields from '@components/FormFields/FormFields';

import styles from './DrawerForm.module.less';

const DrawerForm = ({
  toggleDrawer,
  visible,
  handleSubmit,
  isNew,
  currentRecord,
  schema,
  entity
}) => {
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState(`Crear ${entity}`);
  const [form] = Form.useForm();

  const onClose = () => {
    toggleDrawer();
  };

  useEffect(() => {
    form.resetFields();
    setTitle(`Crear ${entity}`);
    if (show !== visible) {
      setShow(visible);
    }
    if (!isNew) {
      form.setFieldsValue(currentRecord);
      setTitle(`Modificar ${entity} #${currentRecord.id}`);
    }
  });

  const onFinish = (fields) => {
    const formData = {
      id: currentRecord?.id,
      ...Object.fromEntries(
        Object.entries(fields).filter(
          ([key, value]) => value !== currentRecord[key]
        )
      ),
    };
    handleSubmit(formData);
    toggleDrawer();
  };

  return (
    <>
      <Drawer
        title={title}
        width={720}
        onClose={onClose}
        visible={show}
        bodyStyle={{ paddingBottom: 80 }}
        className={styles.Drawer}
        footer={
          <div className={styles.footer}>
            <Button onClick={onClose} className={styles.cancelButton}>
              Cancel
            </Button>
            <Button type="primary" onClick={form.submit}>
              Submit
            </Button>
          </div>
        }
      >
        <Form
          layout="vertical"
          hideRequiredMark
          form={form}
          onFinish={onFinish}
        >
          <FormFields schema={schema} />
        </Form>
      </Drawer>
    </>
  );
};

export default DrawerForm;