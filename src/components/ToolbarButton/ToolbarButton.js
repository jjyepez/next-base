import { useEffect } from "react";

import {
    Button,
    Tooltip,
    Popover
} from "antd";

import styles from './ToolbarButton.module.less';

const popoverWraper = (component, extra, tooltip='') => {
    
    if(!extra) return (
        <Tooltip title={tooltip} placement='left'>
            {component}
        </Tooltip>
    );

    return (
        <Popover
            placement='leftTop'
            content={extra}
        >
            <Tooltip title={tooltip} placement='top'>
                {component}
            </Tooltip>

        </Popover>
    ) 
}

export const ToolbarButton = ({icon, tooltip, extra, listeners={}}) => {

    return popoverWraper(
        <Button
            className={styles.button}
            icon={icon}
            type='text'
            {...listeners}
        />,
        extra,
        tooltip
    )
}

export default ToolbarButton;