import LoginButton from "@components/LoginButton/LoginButton";
import AppLogo from "@fragments/AppLogo/AppLogo";

import { Layout } from "antd";
const { Header } = Layout;

import styles from './ContentHeader.module.less';

export const ContentHeader = ({title, loginRoute, children}) => {

    return (
        <Header theme='dark' className={styles.ContentHeader}>

            <AppLogo />
            
            <div className={styles.title}>{title}</div>

            <div className={styles.nbsp}>&nbsp;</div>
            
            <div>
                {children}
            </div>

            <LoginButton loginRoute={loginRoute} />

        </Header>
    )
}

export default ContentHeader;