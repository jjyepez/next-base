import { useState, useEffect } from "react";
import { Modal } from "antd";

import styles from "./ReadModal.module.less";

const CrudModal = ({ record, visible, setModalVisible, schema, entity }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    setIsModalVisible(visible);
  }, [visible]);

  const handleCancel = () => {
    //setIsModalVisible(false);
    setModalVisible(false);
  };

  //dbg
  console.log({dbg: {schema, record}})

  return (
    <>
      <Modal
        title={`Mostrando ${entity} #${record.id}`}
        visible={isModalVisible}
        onCancel={handleCancel}
        width="50vw"
        destroyOnClose={true}
        footer={null}
        bodyStyle={{
          height: "100%",
          width: "100%",
          padding: "2rem 3rem",
        }}
      >
        <div className={styles.Content}>
          {Object.keys(record).map((key) => {

            const property = schema[key];

            if (property && !property.hideOnRead) {

              return (
                <div key={key}>

                    <div className={styles.label}>
                      {property.title}
                    </div>

                  {
                    property.render ? (

                        <div className={styles.value}>{
                            property.render(record[key])
                            || 
                            <>&nbsp;</>
                        }</div>

                        ) : (
                        <div className={styles.value}>
                            { record[key] || <>&nbsp;</> }
                        </div>
                    )
                  }
                </div>
              );
            }
          })}
        </div>
      </Modal>
    </>
  );
};

export default CrudModal;
