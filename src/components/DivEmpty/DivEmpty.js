import {
    Loader
} from 'react-feather';

import { Empty } from 'antd';

import styles from './DivLoading.module.less';

function DivLoading({text='', style='default', size='2.5rem'}) {

    return (
    
        <div className={styles.DivLoading}>
    
            <div>

                {style === 'antd' ? (
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='Vacío.'/>
                ) : (
                    <>
                        <Loader className='spin' size={size} strokeWidth={1} />
                        <div className={styles.LoadingText}>{text}</div>
                    </>
                )}
                
            
            </div>
    
        </div>
    
    )
}

export default DivLoading;
