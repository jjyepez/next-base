import styles from './Content.module.css';

function Content(props) {
    return (

        <div className={`${styles.Content} container-fluid`}>

            {props.children}
        
        </div>

    )
}

export default Content;
