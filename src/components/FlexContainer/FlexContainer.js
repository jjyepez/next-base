import styles from "./FlexContainer.module.less";

const FlexContainer = ({ children }) => {
  return (
    <div className={styles.FlexContainer}>

      <div>{children}</div>
    
    </div>
  )
}

export default FlexContainer;
