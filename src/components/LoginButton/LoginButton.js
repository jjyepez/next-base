import { useState, useEffect } from "react";
import Router from 'next/router';

import { useCookies } from "react-cookie";

import { Button } from 'antd';

import InfoUser from "../InfoUser/InfoUser";

const LoginButton = ({ loginRoute = '/login' }) => {
  
  const [cookie, setCookie, removeCookie] = useCookies(["IUEtoken"]);
  const [show, setShow] = useState(false);

  const handleLogoutClick = async() => {
    removeCookie("IUEtoken", {
      path: '/',
      sameSite: 'lax'
    });
    setShow(false);
  };

  useEffect(() => {
    setShow(
      Router.pathname !== loginRoute
    )
  }, [Router.pathname])

  return (
    <>
      {show &&

        <>
          <InfoUser/>
      
          <Button
            //size='small'
            type="primary"
            onClick={handleLogoutClick}
          >
            Cerrar sesión

          </Button>
        </>

      } 

    </>
  );
};

export default LoginButton;
