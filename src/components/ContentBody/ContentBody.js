import styles from './ContentBody.module.css';

function ContentBody(props){
    return (
    
        <div className={`${styles.ContentBody} container-fluid`}>
        
            {props.children}
        
        </div>
    
    )
}

export default ContentBody;
