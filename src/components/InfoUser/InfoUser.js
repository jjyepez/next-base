import { useEffect, useState } from "react";

import { useCookies } from "react-cookie";

import styles from "./InfoUser.module.less";

const InfoUser = () => {
  const [infoUser, setinfoUser] = useState({
    nombres: "",
    apellidos: ""
  });

  const [cookie] = useCookies(["IUEtoken"]);

  useEffect(() => {
    const IUEtoken = cookie.IUEtoken;

    if (IUEtoken) {
      setinfoUser({
        ...infoUser,
        ...IUEtoken,
      });
    } else {
      setinfoUser({
        nombres: "",
        apellidos: ""
      });
    }
  }, [cookie]);
  return (
    <>
      {infoUser.nombres && (
        <div className={styles.InfoUser}>
          <span>
            U: {`${infoUser.nombres} ${infoUser.apellidos}`.trim()}
            <br />
            {/*R: {infoUser.rol}*/}
          </span>
        </div>
      )}
    </>
  );
};

export default InfoUser;
