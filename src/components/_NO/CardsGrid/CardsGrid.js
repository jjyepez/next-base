import DivLoading from '@components/DivLoading/DivLoading';

import styles from './CardsGrid.module.less';

function CardsGrid({ data, renderCardHandle }) {

    return (

        data.length === 0 ? (
        
            <DivLoading />
    
        ) : (
    
            <div className={styles.CardsGrid}>
                {
                    data.map(renderCardHandle)
                }
            </div>
    
        )
    )
}

export default CardsGrid;
