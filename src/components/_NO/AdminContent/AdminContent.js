import {
  Layout
} from 'antd';

import styles from "./AdminContent.module.less";

const { Content } = Layout;

const AdminContent = ({ children }) => {
  return (
    <Content className={styles.AdminContent}>
      {children}
    </Content>
  )
}

export default AdminContent
