import { useRouter } from "next/router";

import { Layout } from 'antd';
const { Content, Sider } = Layout;

import ContentHeader from "@components/ContentHeader/ContentHeader";
import SideBar from "@components/SideBar/SideBar";
import TabNav from "@components/TabNav/TabNav";

import styles from "./FlexLayout.module.less";

const renderContent = ({ title, modules, loginRoute, children }) => {

	const router = useRouter();
	const modulePath=`/${router.pathname.split('/')[1]}`;

	const appModules = modules || {};
	const thisModule = appModules[modulePath];
	const subModules = thisModule?.submodules || {};

	const layout = thisModule?.layout || 'simple-layout';

	const contentHeaderProps = {
		title,
		loginRoute
	}
	
	let innerLayout;

	switch (layout) {

		case 'fullwidth-layout':
			innerLayout = (
				<>
					<ContentHeader {...contentHeaderProps} />
					<TabNav modules={modules} />

					<Layout className={styles.Layout}>
						<Content className={styles.Content}>{children}</Content>
					</Layout>
				</>
			);
			break;

		case 'sidebar-layout':
			innerLayout = (
				<>
					<ContentHeader {...contentHeaderProps} />

					<Layout className={styles.Layout}>

						<Sider className={styles.Sider}>
							<SideBar menuConfig={subModules} />

						</Sider>

						<Content className={styles.Content}>
							{children}
						</Content>

					</Layout>
				</>
			);
			break;

		case 'full-layout':
			innerLayout = (
				<>
					<ContentHeader {...contentHeaderProps} />

					<Layout className={styles.Layout}>
						
						<TabNav modules={modules} />

						<Layout>
							<Sider className={styles.Sider}>
								<SideBar menuConfig={subModules} />
								
							</Sider>

							<Content className={styles.Content}>
								{children}
							</Content>
						</Layout>

					</Layout>
				</>
			);
			break;

		case 'simple-layout':
		default:
			innerLayout = (
				<>
					<ContentHeader {...contentHeaderProps} />
					
					<Layout className={styles.Layout}>

						<Content className={styles.Content}>
							{children}
						</Content>

					</Layout>
				</>
			);
	}

	return innerLayout;
}

function FlexLayout(props) {

	return (
		<Layout className={styles.Layout}>

			{renderContent(props)}

		</Layout>
	);
}

export default FlexLayout;
