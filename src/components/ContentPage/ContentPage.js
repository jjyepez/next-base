import { Layout } from 'antd';
const { Content } = Layout;

import styles from './ContentPage.module.css';

function ContentPage(props){

    return (
        
        <Content className={styles.Content}>
        
            {props.children}
        
        </Content>
    
    )
}

export default ContentPage;
