import {
  useState,
  useEffect,
  useContext
} from 'react';

import Link from "next/link";
import { useRouter } from 'next/router';

import { Menu } from "antd";

import styles from "./SideBar.module.less";

import AppContext from '@context/AppContext/Context';

function SideBar({ menuConfig }) {

  const appContext = useContext(AppContext);
  console.log({appContext});

  const [currentSubmoduleIndex, setCurrentSubmoduleIndex] = useState('0');

  const route = useRouter();

  useEffect( () => {
    setCurrentSubmoduleIndex(String(Object.keys(menuConfig).indexOf(route.pathname)));
  }, [route]);
  
  return (

    <Menu
      selectedKeys={[currentSubmoduleIndex]}
      mode="inline"
      className={styles.SideBar}
    >

      { Object.keys(menuConfig).map((route, i) => (
      
        <Menu.Item key={i} icon={menuConfig[route].icon}>

          <Link className="nav-link" href={route} passHref>
            <a className="pl-3">{menuConfig[route].text}</a>
          </Link>
        
        </Menu.Item>

      )) }

    </Menu>
  )
}

export default SideBar;
