import ToolbarButton from "@components/ToolbarButton/ToolbarButton";

import styles from "./Toolbar.module.less";

const Toolbar = ({buttons=[]}) => {

  return (

    <div className={styles.Toolbar}>
      {
        buttons.map((button, i) => {
          
          return (
            <ToolbarButton
              key={i}
              icon={button.icon}
              extra={button.children}
              tooltip={button.tooltip}
              listeners={button.listeners}
            />
          )

        })
      }
    </div>
  )
}

export default Toolbar;
