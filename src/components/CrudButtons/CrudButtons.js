import styles from './CrudButtons.module.css';

function CrudButtons({newRecordHandle}){
    
    return (
    
        <div className={styles.CrudButtons}>

            <button
                onClick={newRecordHandle}
                className='btn btn-sm btn-secondary text-nowrap'
            >
                Crear nuevo registro
            </button>
        
        </div>
    
    )
}

export default CrudButtons;
