import Context from './Context';

import State from './State';
import Reducer from './Reducer';

import { useReducer } from 'react';

const Provider = ({children, ...props}) => {

    const [state, dispatch] = useReducer(Reducer, State);

    return (

        <Context.Provider value={{ state, dispatch }}>
            {children}
        </Context.Provider>
    )

}

export default Provider;