import {
    SET_SESSION,
    DESTROY_SESSION
} from './Actions';

import InitialState from '@context/AppContext/State';

function Reducer (state = InitialState, action = {}) {

    const {
        type,
        payload
    } = action;

    let newState = state;

    switch (type) {

        case SET_SESSION:
            newState = {
                ...state,
                session: payload
            }
            break;

        case DESTROY_SESSION:
            newState = {
                ...state,
                session: null
            }
            break;

        default:
            // newState remains the same as State
    }

    return newState;

}

export default Reducer;
