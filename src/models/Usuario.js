const schema = {
  id: {
    type: "hidden",
    title: "ID",
    dataIndex: "id",
    sorter: true
  },
  user: {
    type: "text",
    title: "Usuario",
    dataIndex: "user",
    sorter: true,
    editable: false,
    rules: [{
      required: true,
      message: "Ingrese usuario"
    }]
  },
  Nombres: {
    type: "hidden",
    hideOnRead: true, 
    key:  "nombres",
    title: "Nombres",
    sorter: true,
    render: (record) => (
      [record.nombres, record.apellidos].join(' ').trim()
    ) 
  },
  password: {
    title: "Contraseña",
    type: "password",
    hideOnRead: true,
    skipEqual: true,
    rules: [{
      required: true,
      message: "Ingrese contraseña" 
    }]
  },
  nombres: {
    title: "Nombres",
    type: "text",
  },
  apellidos: {
    title: "Apellidos",
    type: "text",
  },
  email: {
    title: "Correo electrónico",
    type: "text",
    sorter: true,
    dataIndex:  "email",
    placeholder: "email",
    rules: [{
      type: "email",
      message: "Ingrese un correo electrónico válido"
    }]
  },
  info_contacto: {
    title: "Teléfono de contacto / Whatsapp",
    type: "text",
    placeholder: "ej: +57(300)123.4567",
  },

  rol_id: {
    type: "select",
    title: "Rol de usuario",
    placeholder: "Seleccione rol...",
    parse: true,
    rules: [{
      required: true,
      message: "Seleccione rol de usuario"
    }],
    // Las opciones son cargadas desde la tabla e inyectadas
    options: {},
    render (key) {
      return this.options[key];
    },
  },
  status_id: {
    type: "select",
    title: "Status",
    placeholder: "Seleccione status...",
    parse: true,
    rules: [{
      required: true,
    }],
    options: {
      1: "Activo",
      2: "Suspendido",
      3: "Reiniciar contraseña",
    },
    render (key) {
      return this.options[key];
    },
  },
};

export { schema };
