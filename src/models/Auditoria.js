const schema = {
  id: {
    type: "hidden",
    title: "ID",
    dataIndex: "id",
    width: 50
  },
  fecha: {
    type: "text",
    title: "Fecha",
    dataIndex: "fecha",
    editable: false,
    readOnlyOnEdit: true,
  },
  accion: {
    type: "text",
    title: "Acción",
    dataIndex: "accion",
    editable: false,
    readOnlyOnEdit: true,
  },
  descripcion: {
    type: "text",
    title: "Descripción",
    dataIndex: "descripcion",
    editable: false,
    readOnlyOnEdit: true,
  },
  usuario: {
    type: "text",
    title: "Usuario",
    dataIndex: "usuario",
    editable: false,
    readOnlyOnEdit: true,
  },
};

export { schema };
