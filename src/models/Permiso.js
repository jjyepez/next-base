const schema = {
  id: {
    type: "hidden",
    title: "ID",
    dataIndex: "id"
  },
  name: {
    type: "text",
    title: "Nombre",
    dataIndex: "name",
    rules: [{
      required: true,
      message: "Ingrese nombre de la ruta"
    }],
    editable: false,
    readOnlyOnEdit: true,
  },
  ruta: {
    type: "text",
    title: "Ruta",
    dataIndex: "ruta",
    placeholder: "/ruta",
    rules: [{
      required: true,
      message: "Ingrese URL de la ruta"
    }],
  },
};

export { schema };
