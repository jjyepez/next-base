const schema = {
  id: { 
    type: "hidden",
    title: "ID",
    dataIndex: "id",
},
  name: {
    title: "Nombre",
    type: "text",
    dataIndex: "name",
    rules: [{
      required: true,
      message: "Ingrese nombre del rol"
    }]
  },
  nivel: {
    title: "Nivel",
    type: "number",
    min: 0,
    max: 99,
    dataIndex: "nivel",
    rules: [{
      required: true,
      message: "Ingrese nivel del rol"
    }]
  },
  permisos_denegados: {
    title: "Permisos denegados",
    type: "select",
    mode: "multiple",
    options: {},
    render (keys = []) {
      return keys.length
    },
  }
};

export { schema };