import users from '@data/users';

const auth = {

  user: async (user, password) => {

    let errors = [];

    if (user.trim() == "") {
      errors.push({
        id: "user",
        msg: "El usuario es requerido.",
      });
    }
    if (password.trim() === "") {
      errors.push({
        id: "password",
        msg: "La contraseña es requerida.",
      });
    }

    if (errors.length > 0) {
      return {
        error : true,
        status: 400,
        msg   : "Error en los datos."
      };
    }

    if(users[user.trim()]['pwd'] !== password.trim()){

      return {
        error  : true,
        status : 401,
        payload: {},
      };

    } else {

      const rslt = { // DUMMY
        "json": {
          "status": 200,
          "payload": {
            "user"     : user.trim(),
            "nombres"  : users[user.trim()]['nombre'],
            //"email"    : "admin@correo.com",
            "apellidos": "",
            "status_id": 1,
            "rol_id"   : 1,
            "nivel"    : 50
          },
          "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYWRtaW4iLCJlbWFpbCI6ImFkbWluQGNvcnJlby5jb20iLCJub21icmVzIjoiQWRtaW5pc3RyYWRvciIsImFwZWxsaWRvcyI6Ik5VU0UiLCJzdGF0dXNfaWQiOjEsInJvbF9pZCI6Miwibml2ZWwiOjg5LCJpYXQiOjE2MjMyOTM1NTIsImV4cCI6MTYyMzI5NTM1Mn0.KCgLp0c2oFsr4xBhugRy0oSdHywSIYP9KT8WoVzoTYs",
          "expires": "2021-06-10T03:22:32.054Z"
        }
      }
  
      return {
        error: false,
        status: rslt.json.status,
        payload: rslt.json.payload,
      };

    }

  }

};

export default auth;