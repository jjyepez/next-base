const CRUD = entity => {
    const ENDPOINT = process.env.NEXT_PUBLIC_SERVICES_ENDPOINT + entity;

    return {
        load: async (queryParameters = {}) => {
          const url = new URL(ENDPOINT);
          url.search = new URLSearchParams(queryParameters);
      
          return await fetch(url, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
            },
            credentials: 'include'
          })
            .then((rslt) => rslt.json())
            .then((json) => {
              if (json.error) {
                // TODO: error.output?
                return {
                  error: true,
                  ...json,
                };
              } else {
                return {
                  error: false,
                  ...json,
                };
              }
            })
            .catch((error) => {
              return {
                error: true,
                status: 500,
                msg: error,
              };
            });
        },
      
        upsert: async (data) => {
          return await fetch(ENDPOINT, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            credentials: 'include',
            body: JSON.stringify(data),
          })
            .then((rslt) => rslt.json())
            .then((json) => {
              if (json.error) {
                // TODO: error.output?
                return {
                  error: true,
                  ...json,
                };
              } else {
                return {
                  error: false,
                  ...json,
                };
              }
            })
            .catch((error) => {
              return {
                error: true,
                status: 500,
                msg: error,
              };
            });
        },
      
        delete: async (id) => {
          if (!parseInt(id)) {
            return {
              error: true,
              status: 400,
              msg: "El id es requerido.",
            };
          }
      
          return await fetch(ENDPOINT, {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
            },
            credentials: 'include',
            body: JSON.stringify({
              id,
            }),
          })
            .then((rslt) => rslt.json())
            .then((json) => {
              if (json.error) {
                // TODO: error.output?
                return {
                  error: true,
                  json,
                };
              } else {
                return {
                  error: false,
                  json,
                };
              }
            })
            .catch((error) => {
              return {
                error: true,
                status: 500,
                msg: error,
              };
            });
        },
      }
};

export default CRUD;
