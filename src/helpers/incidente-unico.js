module.exports = {
    querySoloFechas,
    queryFechasANI,
}

async function querySoloFechas({fechaDesde='2021-05-01', fechaHasta='2021-05-18'}={}){
    const top = 200;
    let sql = `
        SELECT TOP ${top}
            RAND(CHECKSUM(NEWID())) AS "key",
            *
        FROM
            DataFeed_Datatraffic.sch_datatraffic.datatraffic
        WHERE
            
            FechaCreacion between CAST('${fechaDesde}' as datetime) AND CAST('${fechaHasta}' as datetime)
            AND
            duracion is not null
    ;`
    .replace(/[\n\r]/ig,' ')
    .replace(/[\s]+/g,' ')
    .trim()
    ;

    //sql = encodeURIComponent(sql);
    let url = `http://10.219.1.141:7500/api/test/mssql/DataFeed_Datatraffic/${sql}`;
    
    let json = await fetch(url, {
            method: 'GET'
        })
        .then(rslt => rslt.json())
        .catch(err => console.log(err))
    ;

    return json;
}

async function queryFechasANI({fechaDesde='2021-05-01', fechaHasta='2021-05-18', ani='3130000000'}={}){
    const aniMod = ani.replace(/[^\d]/ig,'').trim();
    console.log({aniMod});
    const top = 200;
    let sql = `
        SELECT TOP ${top}
            RAND(CHECKSUM(NEWID())) AS "key",
            *
        FROM
            DataFeed_Datatraffic.sch_datatraffic.datatraffic
        WHERE
            
            FechaCreacion between CAST('${fechaDesde}' as datetime) AND CAST('${fechaHasta}' as datetime)
            AND
            duracion is not null
            AND
            TRIM(
                REPLACE(
                    REPLACE(NumeroTelefonoIncidente,CHAR(9), '')
                ,' ','')
            ) = '${aniMod}'
    ;`
    .replace(/[\n\r]/ig,' ')
    .replace(/[\s]+/g,' ')
    .trim()
    ;

    //sql = encodeURIComponent(sql);
    let url = `http://10.219.1.141:7500/api/test/mssql/DataFeed_Datatraffic/${sql}`;
    
    let json = await fetch(url, {
            method: 'GET'
        })
        .then(rslt => rslt.json())
        .catch(err => console.log(err))
    ;

    return json;
}