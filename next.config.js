const withAntdLess = require('next-plugin-antd-less');

const lessToJS = require('less-vars-to-js');
const fs = require('fs');
const path = require('path');

// Where your antd-custom.less file lives
const themeVariables = lessToJS(
  fs.readFileSync(path.resolve(__dirname, './src/styles/antd-theme.less'), 'utf8')
);

nextConfig = {
  
  lessLoaderOptions: {
    
    cssModules: false,
    javascriptEnabled: true,

    modifyVars: {
      ...themeVariables
    } // make your antd custom effective
  },

  webpack(config) {
    
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    });

    return config;
  }
}

module.exports = withAntdLess(nextConfig);
